const socket = io();

function parseCodebeat () {
  socket.emit('parse_codebeat', {
    notes: document.querySelector('#raw-codebeat').value
  })
}

socket.on("parsed_codebeat", (data) => {
  document.querySelector('#parsed-codebeat').value = JSON.stringify(data.notes, 4, '\t')
})

document.addEventListener("input", parseCodebeat)