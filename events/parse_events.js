import Parse from '../models/parse_model'
import { AudioContext } from 'web-audio-api'

// local import for testing, replace with 'codebeat' for deployment
import Codebeat from '../../codebeat/index'

export default class ParseEvents {
  parse_codebeat(data) {
    const parser = new Codebeat({ context: AudioContext })
    const notes = parser.parseNotes(data.notes)
    notes.timestamp = new Date()
    Parse.push(notes)
    this.emit('parsed_codebeat', { notes })
  }
}
