import express from 'express'
import path from 'path'
import { createServer } from 'http'
import socketIO from 'socket.io'
import ParseEvents from './events/parse_events'

const app = express();
const server = createServer(app);
const io = socketIO(server)
const root = __dirname + '/views/'
const parse_events = new ParseEvents()

server.listen(3000, () => { console.log('Server open on 3000')})
app.use('/', express.static('views/'));

app.get('/', (req, res) => {
  res.sendFile('index.html')
})

io.on('connection', (socket) => {
  console.log('connected to server.\n')

  socket.on('parse_codebeat', parse_events.parse_codebeat.bind(socket))

  socket.on('disconnect', () => {
    console.log('Disconnected from server.')
  })
})